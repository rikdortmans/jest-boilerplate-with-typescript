export function splitOnDoubleLetter(word) {
    let splitWord = "";
    let wordArray = [];

    for (let i = 0; i < word.length; i++) {
        splitWord += word[i].toLowerCase();
        if (word[i] === word[i + 1]) {
            wordArray.push(splitWord);
            splitWord = "";
        }
    }

    if (wordArray.length > 0) {
        wordArray.push(splitWord);
    }

    return wordArray;
}