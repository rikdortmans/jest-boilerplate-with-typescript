export function noStrangers(longString) {
    let strMap = new Map()
    let acqMap = new Map();
    let friendMap = new Map();

    const wordArray = longString.split(" ");

    for (let i = 0; i < wordArray.length; i++) {
        if (wordArray[i].search(".")) {
            wordArray[i] = wordArray[i].slice(0, -1);
        }
        wordArray[i] = wordArray[i].toLowerCase();

        if (friendMap.has(wordArray[i])) {
            friendMap.set(wordArray[i], friendMap.get(wordArray[i]) + 1)
        }
        else if (acqMap.has(wordArray[i])) {
            acqMap.set(wordArray[i], acqMap.get(wordArray[i]) + 1)
            
            if (acqMap.get(wordArray[i] > 4)) {
                friendMap.set(wordArray[i], acqMap.get(wordArray[i]))
                acqMap.delete(wordArray[i]);
            }
        }
        else {
            strMap.set(wordArray[i], strMap.get(wordArray[i]) + 1 || 1)

            if (strMap.get(wordArray[i]) > 2) {
                acqMap.set(wordArray[i], strMap.get(wordArray[i]))
                strMap.delete(wordArray[i]);
            }
        }
    }

    return [[...acqMap.keys()], [...friendMap.keys()]];
}