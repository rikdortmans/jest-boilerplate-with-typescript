import { noStrangers } from ".";

const testString1 = "See Spot run. See Spot jump. Spot likes jumping. See Spot fly.";

it('should return [["spot", "see"], []] when given "testString1'
    , () => {
        expect(noStrangers(testString1)).toEqual([["spot", "see"], []]);
    })
