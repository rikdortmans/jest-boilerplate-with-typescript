import { elasticize } from ".";


it("should return 'ANNNNA' if given ANNA", ()=>{
    expect(elasticize("ANNA")).toBe("ANNNNA");
})


it("should return 'KAAYYYAAK' if given KAYAK", ()=>{
    expect(elasticize("KAYAK")).toBe("KAAYYYAAK");
})

it("should return 'X' if given X", ()=>{
    expect(elasticize("X")).toBe("X");
})