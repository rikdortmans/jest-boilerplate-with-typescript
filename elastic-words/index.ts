export function elasticize(word) {
    let charArray;
    let left;
    let pivot;
    let right;
    let elasticWord="";

    if (word.length < 3) {
        return word;
    }

    left = word.substring(0, word.length / 2);
    right = word.substring(word.length / 2, word.length);

    if (right.length > left.length) {
        pivot = right.substring(0, 1)
        right = right.substring(1, right.length);
    }

    for (let i = 0; i < left.length; i++) {
        elasticWord += left[i].repeat(i+1);
    }

    if (pivot != undefined) {
        elasticWord += pivot.repeat(left.length+1);
    }

    for (let i = 0; i < right.length; i++) {
        elasticWord += right[i].repeat(right.length - i);
    }

    return elasticWord;
}