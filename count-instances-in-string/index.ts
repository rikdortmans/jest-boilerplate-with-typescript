/**
 * RegExp (Regular Expression) is used in match()
 * Looks like this normally: /a/g (this counts the amount of 'a' in a string
 * g is the global flags, makes sure it counts all occurrences and not just one
 * match returns an array if something matches or null, thus use || [] to always return array
 * @param character character to count in the string
 * @param string string that needs to be checked
 * @returns number of occurrences of character in string
 */
export function charCount(character, string){
    return (string.match(new RegExp(character, "g")) || []).length;
}

