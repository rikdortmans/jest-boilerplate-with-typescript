export function isJackpot(jackpot) {
    const firstElement = String(jackpot[0]);

    for (let i = 1; i < jackpot.length; i++) {
        const element = String(jackpot[i]);

        if (firstElement != element) {
            return false;
        }
    }
    
    return true;
}