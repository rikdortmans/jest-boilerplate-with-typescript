/**
testJackpot(["@", "@", "@", "@"]) ➞ true
testJackpot(["abc", "abc", "abc", "abc"]) ➞ true
testJackpot(["SS", "SS", "SS", "SS"]) ➞ true
testJackpot(["&&", "&", "&&&", "&&&&"]) ➞ false
testJackpot(["SS", "SS", "SS", "Ss"]) ➞ false
 */

import { isJackpot } from ".";

it("should return true if all elements are symbols identical", () => {
    // at least 1 assertion
    expect(isJackpot(["@","@","@","@"])).toBe(true);
});

it("should return true if all elements are letters and identical", () => {
    // at least 1 assertion
    expect(isJackpot(["abc","abc","abc","abc"])).toBe(true);
});

it("should return true if all elements are letters, identical and capitalized", () => {
    // at least 1 assertion
    expect(isJackpot(["SS", "SS", "SS", "SS"])).toBe(true);
});

it("should return false elements are symbols and do not match", () => {
    // at least 1 assertion
    expect(isJackpot(["&&", "&", "&&&", "&&&&"])).toBe(false);
});

it("should return false if all elements are letter and and do not match", () => {
    // at least 1 assertion
    expect(isJackpot(["SS", "SS", "SS", "ss"])).toBe(false);
});