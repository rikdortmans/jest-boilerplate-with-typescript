const MONTHS = {
    1: "A", 2: "B", 3: "C", 4: "D", 5: "E", 6: "H", 7:
        "L", 8: "M", 9: "P", 10: "R", 11: "S", 12: "T"
}

export function generateFiscalCode(personalData) {
    let fiscalCode = "";

    //GENERATE 3 CAPITAL LETTERS FROM SURNAME
    const surname = personalData.surname;
    const surnameConsonants = getConsonants(surname);

    //Name less than 3 letters gets X
    if (surname.length < 3) {
        fiscalCode += surname.toUpperCase() + "X";
    }

    else if (surnameConsonants.length >= 3) {
        fiscalCode += surnameConsonants[0].toUpperCase();
        fiscalCode += surnameConsonants[1].toUpperCase();
        fiscalCode += surnameConsonants[2].toUpperCase();
    }
    else if (surnameConsonants.length < 3) {
        const surnameVowels = getVowels(surname);
        fiscalCode += surnameConsonants.join("").toUpperCase();
        for (let i = 0; fiscalCode.length < 3; i++) {
            fiscalCode += surnameVowels[i].toUpperCase();
        }
    }

    // GENERATE 3 CAPITAL LETTERS FROM NAME
    const name = personalData.name;
    const nameConsonants = getConsonants(name);
    if (name < 3) {
        fiscalCode += name.toUpperCase() + "X".repeat(3 - name.length);
    }
    else if (nameConsonants.length == 3) {
        fiscalCode += nameConsonants.join("").toUpperCase();

    }
    else if (nameConsonants.length > 3) {
        fiscalCode += nameConsonants[0].toUpperCase();
        fiscalCode += nameConsonants[2].toUpperCase();
        fiscalCode += nameConsonants[3].toUpperCase();

    }
    else if (nameConsonants.length < 3) {
        const nameVowels = getVowels(name);
        fiscalCode += nameConsonants.join("").toUpperCase();
        for (let i = 0; fiscalCode.length < 6; i++) {
            fiscalCode += nameVowels[i];
        }
    }

    // GENERATE 2 NUMBERS, 1 LETTER, 2 NUMBERS FROM DOB AND GENDER
    const dateOfBirth = personalData.dob.split("/");
    const gender = personalData.gender;

    fiscalCode += dateOfBirth[2].slice(-2);
    fiscalCode += MONTHS[dateOfBirth[1]];

    switch (gender) {
        case "M":
            if (dateOfBirth[0] < 10) {
                fiscalCode += "0" + dateOfBirth[0];
            }
            else {
                fiscalCode += dateOfBirth[0];
            }
            break;
        case "F":
            fiscalCode += (parseInt(dateOfBirth[0]) + 40).toString();
            break;
        default:
            break;
    }

    return fiscalCode;
}

const getConsonants = (str) => (str.match(/[^aeiou]/gi));
const getVowels = (str) => (str.match(/[aeiou]/gi));