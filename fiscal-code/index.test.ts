import { generateFiscalCode } from ".";

const personalData1 = {
    name: "Matt",
    surname: "Edabit",
    gender: "M",
    dob: "1/1/1900"
}

const personalData2 = {
    name: "Helen",
    surname: "Yu",
    gender: "F",
    dob: "1/12/1950"
}

const personalData3 = {
    name: "Mickey",
    surname: "Mouse",
    gender: "M",
    dob: "16/1/1928"
}

it("should return DBTMTT00A01 if given personalData1", () => {
    // at least 1 assertion
    expect(generateFiscalCode(personalData1)).toBe("DBTMTT00A01");
});

it("should return YUXHLN50T41 if given personalData2", () => {
    // at least 1 assertion
    expect(generateFiscalCode(personalData2)).toBe("YUXHLN50T41");
});
it("should return MSOMKY28A16 if given personalData2", () => {
    // at least 1 assertion
    expect(generateFiscalCode(personalData3)).toBe("MSOMKY28A16");
});