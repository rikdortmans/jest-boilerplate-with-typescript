export function points(twoPointers, threePointers) {
    if(twoPointers < 0)
    {
        throw new Error("Negative two points");
    }

    if(threePointers < 0){
        throw new Error("Negative three points");
    }
    return (twoPointers * 2) + (threePointers * 3);
}