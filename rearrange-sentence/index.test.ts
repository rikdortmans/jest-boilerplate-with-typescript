import { rearrange } from ".";
const sentence1 = "is2 Thi1s T4est 3a";
const sentence2 = "4of Fo1r pe6ople g3ood th5e the2";
const sentence3 = "5weird i2s JavaScri1pt dam4n so3";
const sentence4 = " ";

it("should return sentence1 without letters in the correct ", ()=>{
    expect(rearrange(sentence1)).toBe("This is a Test")
})
it("should return sentence2 without letters in the correct ", ()=>{
    expect(rearrange(sentence2)).toBe("For the good of the people")
})
it("should return sentence3 without letters in the correct ", ()=>{
    expect(rearrange(sentence3)).toBe("JavaScript is so damn weird")
})
it("should return sentence4 without letters in the correct ", ()=>{
    expect(rearrange(sentence4)).toBe("")
})