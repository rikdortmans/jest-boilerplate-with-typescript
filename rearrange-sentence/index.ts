export function rearrange(sentence) {

    let wordMap = new Map();
    let makingSentence = true;
    let rearrangedSentence = "";

    if (sentence === " ") {
        return rearrangedSentence;
    }
    const splitSentence = sentence.split(" ");
    
    for (let i = 0; i < splitSentence.length; i++) {
        const placeInSentence = parseInt(splitSentence[i].match(/(\d+)/));
        const noNumberWord = splitSentence[i].replace(/[0-9]/g, '');
        wordMap.set(placeInSentence, noNumberWord);
    }

    let wordMapKey = 1;
    while (makingSentence) {
        rearrangedSentence += wordMap.get(wordMapKey);
        wordMap.delete(wordMapKey);
        if (wordMap.size === 0) {
            makingSentence = false;
        }
        else {
            wordMapKey++;
            rearrangedSentence += " ";
        }
    }

    return rearrangedSentence;
}