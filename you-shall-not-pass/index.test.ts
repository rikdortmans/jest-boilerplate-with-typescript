import { passwordStrengthCheck } from "./index";

it("should return Invalid if given stonk", ()=>{
    expect(passwordStrengthCheck("stonk")).toBe("Invalid");
})

it("should return Invalid if given pass word", ()=>{
    expect(passwordStrengthCheck("pass word")).toBe("Invalid");
})

it("should return Weak if given password", ()=>{
    expect(passwordStrengthCheck("password")).toBe("Weak");
})

it("should return Weak if given 11081992", ()=>{
    expect(passwordStrengthCheck("11081992")).toBe("Weak");
})

it("should return Moderate if given mySecurePass123", ()=>{
    expect(passwordStrengthCheck("mySecurePass123")).toBe("Moderate");
})

it("should return Moderate if given !@!pass", ()=>{
    expect(passwordStrengthCheck("!@!pass")).toBe("Moderate");
})

it("should return Strong if given @S3cur1ty", ()=>{
    expect(passwordStrengthCheck("@S3cur1ty")).toBe("Strong");
})